#!/usr/bin/env python

import pandas as pd
import pyproj as pj


data = pd.io.parsers.read_csv('traffic-signals-08-01-14.csv')

east = data['Eastings'].values
north = data['Northings'].values

bng = pj.Proj(init='epsg:27700')
wgs84 = pj.Proj(init='epsg:4326')

lats = []
longs = []

for i in range(len(east)):
    e = east[i]
    n = north[i]
    lon,lat = pj.transform(bng,wgs84, e, n)
    
    longs.append(lon)
    lats.append(lat)

data['Latitude'] = lats
data['Longitude'] = longs

data.to_csv('traffic-converted-lat-lon.csv', index=False)



    
