#!/bin/bash

file_list=2014*.csv

function getData { 
    tail -n $(expr $(wc -l $1 | cut -d " " -f 1) - 1) $1  
}

rm collected.csv
for i in $file_list; do 
    hash=$(echo $i | md5sum | cut -d " " -f 1) 
    getData $i | sed "s/\r/,$hash/g" >> collected.csv
done
