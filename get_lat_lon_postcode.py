#!/usr/bin/env python

import pandas as pd
import gmaps

API_KEY=''

data = pd.io.parsers.read_csv('./glasgow-schools-all-denominations-2014-05-01.csv')

address = data['AddressOne']
postcode = data['Postcode']

# FIXME: Load API key from a file
c = gmaps.geocoding.Geocoding(api_key=API_KEY)

lats = []
lons = []

for i in range(len(address)):
    a = address[i]
    p = postcode[i]

    print(a)
    try:
        res = c.geocode(address=a)
        
        latAndLong = res[0]['geometry']['location']

        lats.append(latAndLong['lat'])
        lons.append(latAndLong['lng'])
    except:
        lats.append(666)
        lons.append(666)

data['Latitude'] = lats
data['Longitudes'] = lons

data.to_csv('schools-with-lons.csv', index=False)
